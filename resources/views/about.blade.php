@extends('layout.mainlayout')

@section('container')
<div class="judul fst-italic fw-bold">
    <h1>About Me</h1>
</div>
<div class="deskripsi fst-italic">
    <p>{{ $title }}</p>
</div>
<div class="secondry">

</div>
<div class="skill fst-italic fw-bold">
    <h1>Skills</h1>
</div>
<div class="list">
    <dl>
        <dt class="pb-3"><i class="bi bi-bootstrap me-2 text-dark"></i>Bootstrap</dt>
        <dt class="pb-3"><i class="fab fa-html5 me-2 text-dark"></i>HTML</dt>
        <dt class="pb-3"><i class="fab fa-css3-alt me-2 text-dark"></i>CSS</dt>
    </dl>
</div>
@endsection
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="css/second.css">

    <!-- icon bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" />

    <title>Dannifiah | {{ $jdl }}</title>
  </head>

  <body>

    <!-- Navbar -->
<header id="header">
    <div class="d-flex flex-colum">
        <!-- 
        <img src="img/fotoku.jpg" alt="" width="80" height="80">
        -->
        <p class="fs-3 fw-bold fst-italic text-dark" style="margin-top: 20px ">DANIAR<span style="color: rgb(255, 255, 255)">annifiah</span></p>
    </div>
    <div class="pilihan fw-bold fst-italic fixed-top" >
        <ul class="navbar-nav">
            <li class="nav-item pb-3">
                <a class= "nav-link text-light" href="/"><i class="bi bi-house-door me-2 "></i>Home</a>
            </li>
            <li class="nav-item pb-3 text-light">
                <a class="nav-link text-light" href="/about"><i class="bi bi-person me-2 "></i>About</a>
            </li>
            <li class="nav-item pb-3">
                <a class="nav-link text-light" href="/project"><i class="bi bi-card-list"></i> Project</a>
            </li>
            <li class="nav-item pb-3">
                <a class="nav-link text-light" href="/contact"><i class="bi bi-envelope-open me-2 "></i>Contact</a>
            </li>
        </ul>
    </div>
    <button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>
    <div class="production">
        <ul class="copyright text-dark fw-dark nav-link">
            <p class="text-dark fs-6">&copy; <i><b>DANIAR</b></i><span style="color: rgb(254, 255, 255)"><i>annifiah</i></span></p>
            <p class="huss text-dark  fs-6 mt-n2">Created With <i class="bi bi-heart-fill text-danger"></i> By
                <a class="text-dark" href="https://www.instagram.com/daniar.annifiah/"><i><b>Daniar Istianatul Wafiah</b></i></a></p>
        </ul>
    </div>
</header>
<!-- END -->
<div class="container">
    <div class="slide">
        <div class="slide1"></div>
        <div class="slide2"></div>
        <div class="slide3"></div>
    </div>
    <h1 class="display-5 pt-4 text-dark fw-bold fst-italic" style="margin-top: 8.6rem; margin-left:8.2rem;  color: rgba(255, 132, 245, 0.919);
    text-shadow: -1px 0 #ff9999, 0 2px #ff9999, 2px 0 #ff9999, 0 -1px #ff9999;">Daniar Istianatul Wafiah</h1>   
</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
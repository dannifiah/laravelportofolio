@extends('layout/mainlayout')
    @section('container')
        <div class="prjk">      
            <div class="row text-center mb-3">
              <div class="col fst-italic" data-aos="fade-down" data-aos-delay="100">
                <h3>My Project</h3>
              </div>
            </div>
            <div class="row">
              <!-- max 12 space sehingga menggunakan 4 = 12 : 4 sehingga menampung 3 baris colom -->
              <div class="col-md-6 mb-3" style="box-shadow: 0 4px 8px 0 rgb(218, 61, 152), 0 6px 20px 0 rgb(218, 61, 152);">
                <div class="card" data-aos="zoom-in" data-aos-delay="300">
                  <img src="img/G1.jpg" class="card-img-top" alt="hobi1" />
                  <div class="card-body">
                    <p class="card-text text-center fw-bold" style="font-size: 20px">{{ $textproject1 }}</p>
                  </div>
                </div>
              </div>
              <div class="col-md-6 mb-3" style="box-shadow: 0 4px 8px 0 rgb(218, 61, 152), 0 6px 20px 0 rgb(218, 61, 152);">
                <div class="card" data-aos="zoom-in" data-aos-delay="400">
                  <img src="img/G3.jpg" class="card-img-top" alt="hobi2" />
                  <div class="card-body text-center">
                    <p class="card-text fw-bold">{{ $textproject2 }}</p>
                  </div>
                </div>
              </div>
              <div class="col-md-6 mb-3" style="box-shadow: 0 4px 8px 0 rgb(218, 61, 152), 0 6px 20px 0 rgb(218, 61, 152);">
                <div class="card" data-aos="zoom-in" data-aos-delay="500">
                  <img src="img/G2.jpg" class="card-img-top" alt="hobi3" />
                  <div class="card-body text-center">
                    <p class="card-text fw-bold">{{ $textproject3 }}</p>
                  </div>
                </div>
              </div>
              <div class="col-md-6 mb-3" style="box-shadow: 0 4px 8px 0 rgb(218, 61, 152), 0 6px 20px 0 rgb(218, 61, 152);">
                  <div class="card pt-2" data-aos="zoom-in" data-aos-delay="600">
                    <img src="img/G4.png" class="card-img-top" alt="hobi3" />
                    <div class="card-body text-center">
                      <p class="card-text fw-bold">{{ $textproject4 }}</p>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </section>
@endsection